import React from "react";
import Component from "react-component-component";
import { withRouter } from "react-router-dom";
import "./index.css";
import { AuthPreloader } from "./AuthPreloader";

export const _Auth = ({ setUser, history }) => (
  <Component
    initialState={{
      email: "",
      password: ""
    }}
  >
    {({ state, setState }) => {
      const user = {
        email: state.email,
        password: state.password
      };
      const userAuth = async user => {
        setState({ loading: true });
        const resp = await fetch("http://192.168.0.118:3001/profile/login", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(user)
        });
        setState({ loading: false });
        try {
          if (resp.ok) {
            const userData = await resp.json();
            localStorage.setItem("user", JSON.stringify(userData));
            setUser(userData);
            history.push("/purchases");
          } else {
            const { message } = await resp.json();
            throw new Error(message);
          }
        } catch (e) {
          setState({ error: e.message });
        }
      };
      return (
        <div className="auth">
          <div className="auth__logo">My Shop</div>
          <div className="auth__input-group">
            {state.error && (
              <div className="error">{state.error} , try again</div>
            )}
            <div className="auth__input-group--title">email</div>
            <input
              className="auth__input-group--input"
              value={state.email}
              onChange={({ target: { value } }) => setState({ email: value })}
              type="text"
            />
            <div className="auth__input-group--title">password</div>
            <input
              className="auth__input-group--input"
              value={state.password}
              onChange={({ target: { value } }) =>
                setState({ password: value })
              }
              type="password"
            />
            <button
              onClick={() => userAuth(user)}
              className="auth__input-group--sign-in"
            >
              {state.loading ? <AuthPreloader /> : "Sign in"}
            </button>
            <span className="auth__have-not-account-text">
              Have not account?
            </span>
            <button className="auth__sign-up">Sign up</button>
          </div>
        </div>
      );
    }}
  </Component>
);


export const Auth = withRouter(_Auth);
