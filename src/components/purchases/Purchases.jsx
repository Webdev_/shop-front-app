import React,{Fragment} from 'react'
import { Header } from '../header/Header';
import { CreatePurchases } from '../header/CreatePurchases';
import { PurchasesList } from './PurchasesList';
import Component from 'react-component-component'

export const Purchases = ({title , user}) =>(
    <Component initialState={{
        purchases:[],
        loading:true
    }}
    didMount={async({setState})=>{
        const resp = await fetch("http://192.168.0.118:3001/purchases/list",{
            method:"GET",
            headers:{
                "Authorization":`${user.token}`,
                "Content-Type":"application/json"
            }
        })
        setState({loading:false})
        try{
            if(resp.ok){
                const respValue = await resp.json();
                setState({purchases:respValue.purchases})
            }
        }catch(e){
            console.log(e)
        }
    }}
    > 
    {({state,setState})=>{
        return(
            <Fragment>
            <Header title={title} />
            <CreatePurchases purchases={state.purchases} setPurchase={setState} user={user} />
            <PurchasesList user={user} delPurchase={setState} loading={state.loading} purchases={state.purchases}/>
        </Fragment>
        )
    }}
  
    </Component>
)