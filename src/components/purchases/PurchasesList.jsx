import React from "react";
import edit from "./edit.svg";
import del from "./delete.svg";
import close from "./close.svg";
import "./styles/index.css";
import Component from "react-component-component";

export const PurchasesList = ({ purchases, loading, user, delPurchase }) => (
  <div className="container purchases-list">
    {loading && <p>Loading</p>}
    {!loading && purchases.length > 0 ? (
      purchases.map(({ id, name }) => (
        <Purchase
          purchases={purchases}
          delPurchase={delPurchase}
          key={id}
          user={user}
          id={id}
          name={name}
        />
      ))
    ) : (
      <p className="purchases-list__empty">Nothing to buy</p>
    )}
  </div>
);

export const Purchase = ({ id, name, user, delPurchase, purchases }) => (
  <Component
    initialState={{
      selected: false
    }}
  >
    {({ state, setState }) => {
      const deletePurchase = async id => {
        const purchase = {
          id: id
        };
        const resp = await fetch("http://192.168.0.118:3001/purchases/remove", {
          method: "DELETE",
          headers: {
            Authorization: `${user.token}`,
            "Content-Type": "application/json"
          },
          body: JSON.stringify(purchase)
        });
        const filterById = arr => arr.filter(obj => obj.id !== purchase.id);
        delPurchase({ purchases: filterById(purchases) });
        try {
          if (resp.ok) {
            const respValue = await resp.json();
            // console.log(filterById(respValue))
            // console.log(respValue)
          }
        } catch (e) {
          console.log(e);
        }
      };
      const selectPurchase = () =>
        setState({ selected: state.selected ? false : true });
      return (
        <div className="purchase">
          <ul>
            <li className="purchase__name">{name}</li>
            <li className="purchase__tools">
              <button>
                <img src={edit} alt="" />
              </button>
            </li>
            <li className="purchase__tools">
              <button onClick={selectPurchase}>
                <img src={del} alt="" />
              </button>
            </li>
          </ul>
          <button
            onClick={() => deletePurchase(id)}
            className={`purchase__delete ${state.selected && "active"}`}
          >
            Delete
          </button>
          <button
            onClick={selectPurchase}
            className={`purchase__close ${state.selected && "active"}`}
          >
            <img src={close} alt="" />
          </button>
        </div>
      );
    }}
  </Component>
);
