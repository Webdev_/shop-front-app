import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Auth } from "./components/auth/Auth";
import "./styles/index.css";
import Component from "react-component-component";
import {Purchases} from './components/purchases/Purchases'

function setUserFromLocalStorage() {
  const user = localStorage.getItem("user");
  if (user) {
    try {
      return JSON.parse(user);
    } catch (e) {
      console.log("Cannot deserialize user", e);
      return null;
    }
  } else {
    return null;
  }
}

const App = () => (
  <Component
    initialState={{
      user: setUserFromLocalStorage()
    }}
  >
    {({ state, setState }) => {
      const setUser = user => {
        setState({ user });
      };
      return (
        <Router>
          <Switch>
            <Route
              exact
              path="/"
              render={props => <Auth setUser={setUser} />}
            />
            {state.user && <Route path="/purchases" render={props=><Purchases user={state.user} title="My purchases" />} /> }
          </Switch>
        </Router>
      );
    }}
  </Component>
);

export default App;
