import React from "react";
import Component from "react-component-component";
import create from "./create.svg";
import pointer from './pointer.svg';

export const CreatePurchases = ({ user , setPurchase , purchases }) => (
  <Component
    initialState={{
      name: ""
    }}
  >
    {({ state, setState }) => {
      const purchase = {
        name: state.name
      };
      const createPurchase = async purchase => {
        const resp = await fetch("http://192.168.0.118:3001/purchases/new", {
          method: "POST",
          headers: {
            Authorization: `${user.token}`,
            "Content-Type":"application/json"
          },
          body: JSON.stringify(purchase)
        });
        console.log(purchase)
        try {
          if (resp.ok) {
            const respValue = await resp.json();
            setPurchase({purchases:[...purchases , respValue.list]});
          }
        } catch (e) {
          console.log(e);
        }
      };
      const isValidName = state.name.length > 0;
      return (
        <div className="create-purchases">
          <div className="create-purchases-wrapper">
            <input
              className="create-purchases-wrapper__input"
              type="text"
              value={state.name}
              onChange={({ target: { value } }) => setState({ name: value })}
              placeholder="Create purchase list..."
            />
            <button disabled={!isValidName} onClick={()=>createPurchase(purchase)} className="create-purchases-wrapper__create">
              <img src={create} alt="" />
            </button>
          </div>
          <img className="create-purchases__pointer" src={pointer} alt=""/>
        </div>
      );
    }}
  </Component>
);
