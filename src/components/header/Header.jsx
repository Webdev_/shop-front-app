import React , {Fragment} from "react";
import { withRouter } from "react-router-dom";
import "./styles/index.css";
import menu from "./menu.svg";
import Component from "react-component-component";

export const _Header = ({ title }) => {
  return (
    <Component
      initialState={{
        isAccountOpened: false
      }}
    >
      {({ state, setState }) => {
        const openAccount = () =>setState({isAccountOpened:state.isAccountOpened ? false : true})
        return (
          <Fragment>
          <div className="header">
            <div className="container-fluid">
              <div className="row">
                <div className="col-2" />
                <div className="col-8">
                  <div className="header__title">{title}</div>
                </div>
                <div className="col-2">
                  <button onClick={openAccount} className="header__account">
                    <img src={menu} alt="" />
                  </button>
                  
                </div>
              </div>
            </div>
          </div>
          <UserAccount isOpened={state.isAccountOpened} />
          </Fragment>
        );
      }}
    </Component>
  );
};

export const _UserAccount = ({history , isOpened}) => {
  const logout = () =>{
    localStorage.removeItem("user");
    history.push("/")
  }
  return(
  <div className={`user-account ${isOpened && "visible"}`}>
    <button onClick={logout} className="user-account__logout">Logout</button>
  </div>
  )
};

export const UserAccount = withRouter(_UserAccount);

export const Header = withRouter(_Header);